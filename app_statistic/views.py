from django.shortcuts import render
from app_status.models import Update
from app_AddFriend.models import Friend
# Create your views here.

def index(request):
	response = {}
	response['picture'] = "https://i.imgur.com/fy18KSL.jpg"
	response['friends'] = Friend.objects.all().count()
	response['title'] = 'My Stats'
	html = 'app_statistic/app_statistic.html'
	feedCount = Update.objects.all().count()
	response['feed'] = feedCount
	if(feedCount<1):
		response['status'] = 'Tidak ada status'
	else:
		response['status'] = Update.objects.latest('created_date')
	return render(request, html, response)