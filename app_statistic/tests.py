from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from app_status.models import Update as feed
from app_AddFriend.models import Friend as friend

# Create your tests here.
class AppStatsUnitTest(TestCase):

	def test_app_stats_url_is_exist(self):
		response = Client().get('/app_stats/')
		self.assertEqual(response.status_code,200)

	def test_app_stats_using_index_func(self):
		found = resolve('/app_stats/')
		self.assertEqual(found.func, index)

	def test_content_is_updated(self):
		testMessage = 'Halo Imbang'
		latestFeed = feed.objects.create(message=testMessage)
		request = Client().get('/app_stats/')
		response = index(request)
		html_response = response.content.decode('utf8')
		feedsAmount = str(feed.objects.all().count())+" Post"
		friendAmount = str(friend.objects.all().count())+" People"
		self.assertIn(friendAmount, html_response)
		self.assertIn(feedsAmount, html_response)
		self.assertIn(testMessage, html_response)


