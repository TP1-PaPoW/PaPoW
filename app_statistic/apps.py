from django.apps import AppConfig


class AppStatisticConfig(AppConfig):
    name = 'app_statistic'
