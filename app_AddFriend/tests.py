from django.test import Client
from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from .models import Friend
from .forms import Friend_Form
from .views import index, add_friend

class addFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/app_addFriend/')
		self.assertEqual(response.status_code, 200)
	def test_add_friend_using_index(self):
		found = resolve('/app_addFriend/')
		self.assertEqual(found.func,index)
	def test_add_friend_create_new_friend(self):
		new_activity = Friend.objects.create(name='aku',URL='http://google.com') #buat baru
		count_friend = Friend.objects.all().count() #hitung semua objek
		self.assertEqual(count_friend,1) #kalo jadi, jumlah friend == 1

	def test_showing_all_friends(self):
		name_one = 'One'
		url_one = 'https://one.herokuapp.com'
		data_one = {'name':name_one, 'url':url_one}
		add_friend_one = Client().post('/app_addFriend/', data_one)
		self.assertEqual(add_friend_one.status_code, 200)

		name_two = 'Two'
		url_two = 'https://two.herokuapp.com'
		data_two = {'name':name_two, 'url':url_two}
		add_friend_two = Client().post('/app_addFriend/', data_two)
		self.assertEqual(add_friend_two.status_code, 200)

	def test_str_equal_to_name(self):
		new_activity = Friend.objects.create(name='aku',URL='http://google.com')
		self.assertEqual(new_activity.name,new_activity.__str__())
	
	def test_if_addFriend_Success(self):
		name = 'Jason'
		URL = 'ign@gmail.com'
		response = Client().post('/app_addFriend/add_friend', {'name': name, 'URL': URL})
		self.assertEqual(response.status_code, 302)
		response= Client().get('/app_addFriend/')
		html_response = response.content.decode('utf8')
		self.assertIn(name, html_response)
		self.assertIn(URL, html_response)
	
	def test_if_addFriend_Fail(self):
			name = 'Jason'
			URL = 'ign@gmail.com'
			response = Client().post('/app_addFriend/add_friend', {'name': '', 'URL': ''
				})
			self.assertEqual(response.status_code, 302)
			response= Client().get('/app_addFriend/')
			html_response = response.content.decode('utf8')
			self.assertNotIn(name, html_response)
			self.assertNotIn(URL, html_response)		