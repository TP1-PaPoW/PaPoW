from django.shortcuts import render
from datetime import datetime, date
from .models import Update
from django.http import HttpResponseRedirect
from .forms import Message_Form

response = {'author': "Misael Jonathan"}
curr_year = int(datetime.now().strftime("%Y"))
timeline_status = {}
def index(request):
    response = {}
    response['message_form'] = Message_Form
    message = Update.objects.all()
    response['message'] = message
    response['todo_form'] = Message_Form
    return render(request, 'app_status.html', response)

def status_post(request):
    form = Message_Form(request.POST or None)
    response['message_form'] = Message_Form
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        message = Update(message=response['status'])
        message.save()
        return HttpResponseRedirect('/app_status/')
    else:
        return HttpResponseRedirect('/app_status/')

# def comment_post(request, post):
#     query = Update.objects.get(pk=post)
#     response['comment_form'] = Comment_Form
#     if(request.method == 'POST' and form.is_valid()):
#         response['comment'] = request.POST['comment']
#         comment = Update(message=response['comment'])
#         comment.save()
#         return HttpResponseRedirect('/app_status/')
#     else:
#         return HttpResponseRedirect('/app_status/')
