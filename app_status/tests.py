from .models import Update
from .views import index
from django.test import TestCase
from django.test import Client
from django.urls import resolve

class AppUpdateStatusUnitTests(TestCase):

    def test_app_status_is_exist(self):
        response = Client().get('/app_status/')
        self.assertEqual(response.status_code, 200)

    def test_app_use_index_func(self):
        found =resolve('/app_status/')
        self.assertEqual(found.func, index)

    def test_app_status_can_post_a_status(self):
        new_activity = Update.objects.create(message="test post status")
        count_all_update = Update.objects.all().count()
        self.assertEqual(count_all_update, 1)

    def test_app_status_post_success_and_render_the_result(self):
        test = 'HaiHai'
        response = Client().post('/app_status/status_post/', {'status': test})
        self.assertEqual(response.status_code, 302)
        response = Client().get('/app_status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def testkosong(self):
        testKosong = "Empty"
        response = Client().post('/app_status/status_post/', {'status': ""})
        self.assertEqual(response.status_code, 302)
        response= Client().get('/app_status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(testKosong, html_response)
