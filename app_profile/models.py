from django.db import models

#DATABASE
class profileMenu(models.Model):
    photo_url = models.URLField(default = "https://i.imgur.com/fy18KSL.jpg");
    name = models.CharField(max_length=234)
    birthday = models.CharField(max_length=100)
    gender = models.CharField(max_length=10)
    description = models.TextField()
    email = models.EmailField()

class Expertise(models.Model):
    user = models.ForeignKey(profileMenu, related_name= "expertises", on_delete= models.CASCADE)
    name = models.CharField(max_length=234)    
