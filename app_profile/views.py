from django.shortcuts import render
from app_profile.models import profileMenu
# Create your views here.

#ngatur fungsi
#response = {'author': "PaPoW"} #TODO Implement yourname
#bio = [{'subject' : 'Birthday', 'value' : '05 Oct'},
      # {'subject' : 'Gender', 'value' : 'Unknown'},
       #{'subject' : 'Expertise', 'value' : 'Django, HTMLL, CSS'},
       #{'subject' : 'Description', 'value' : 'grup tp1 PPW asique'},
       #{'subject' : 'Email', 'value' : 'tp_PaPoW@gmail.com'},
#]
def index(request):
        profile = profileMenu.objects.first()
        expertises = profile.expertises.all()
        html = 'app_profile/app_profile.html'
        response = {
            'profile': profile,
            'expertises': expertises
        }
        return render(request, html, response)
