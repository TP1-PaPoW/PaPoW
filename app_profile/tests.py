from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from django.utils.html import escape
from django.urls import resolve
from app_profile.models import profileMenu, Expertise
from app_profile.views import index

# Create your tests here.

def create_user():
	return profileMenu.objects.create(name="hamam wulan", photo_url="https://i.imgur.com/fy18KSL.jpg", gender="female", description="CS UI 2017 student", email="hamam.wulan@ui.ac.id", birthday="24 Oct")

def create_expertises(user):
	return Expertise.objects.bulk_create([
			Expertise(name="writing", user=user),
			Expertise(name="reading", user=user),
			Expertise(name="typing", user=user),
			])

class AppProfileUnitTest(TestCase):
    def test_get_profile_success(self):
        user = create_user()
        expertises = create_expertises(user)

        response_get = Client().get(reverse('profile:index'))
        html_response = response_get.content.decode('utf8')
        self.assertIn(user.name, html_response)
        self.assertIn(escape(user.photo_url), html_response)
        self.assertIn(user.gender, html_response)
        self.assertIn(user.description, html_response)
        self.assertIn(user.email, html_response)

        for expertise in expertises:
            self.assertIn(expertise.name, html_response)

    def test_use_index_function(self):
        response = resolve(reverse('profile:index'))
        self.assertEqual(response.func, index)
